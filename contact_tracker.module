<?php

define('CT_SEPARATOR_ABOVE', '- Start of Contact Tracker Info -');
define('CT_SEPARATOR_BELOW', '- End of Contact Tracker Info -');

/**
 * Implementation of hook_menu().
 */
function contact_tracker_menu() {
  $items['admin/settings/contact_tracker'] = array(
    'title' => t('Contact tracker settings'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('contact_tracker_admin_settings'),
    'access arguments' => array('administer site configuration'),
  );
  
  $items['admin/settings/contact_tracker/main'] = array(
    'title' => t('Main settings'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -100,
  );
  
  return $items;
}

function contact_tracker_admin_settings() {
  $form = array();
  
  $form['ct_separator_above'] = array(
    '#title' => t('Separator shows above contact tracker information in email'),
    '#type' => 'textarea',
    '#default_value' => variable_get('ct_separator_above', CT_SEPARATOR_ABOVE),
  );
  
  $form['ct_separator_below'] = array(
    '#title' => t('Separator shows below contact tracker information in email'),
    '#type' => 'textarea',
    '#default_value' => variable_get('ct_separator_below', CT_SEPARATOR_BELOW),
  );
  
  return system_settings_form($form);
}


/**
 * Implementation of hook_form_alter().
 */
function contact_tracker_form_alter(&$form, $form_state, $form_id) {
  
  // Alter form contact-admin-edit.
  if ('contact_admin_edit' === $form_id) {
    // Get contact tracker profile for setting default values.
    $contact_tracker_profile = contact_tracker_get_profile($form['cid']['#value']);
    
    $form['contact_tracker'] = array(
      '#type' => 'fieldset',
      '#title' => t('Contact tracker'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#weight' => 100,
      '#tree' => TRUE,
    );
    
    // Load options from modules.
    $hook = 'contact_tracker_option';
    foreach (module_implements($hook) as $module) {
      $function = $module .'_'. $hook;
      
      // The return value is supposed to be a string as a short description.
      $options[$module] = call_user_func($function);
    }
    
    $form['contact_tracker']['options'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Append tracking information into contact email'),
      '#options' => $options,
      '#default_value' => $contact_tracker_profile['options'] ? $contact_tracker_profile['options'] : array(),
    );
    
    $form['contact_tracker']['position'] = array(
      '#type' => 'radios',
      '#title' => t('Append extra information at'),
      '#options' => array(
        'top' => t('The beginning of email body.'),
        'bottom' => t('The end of email body.'),
      ),
      '#default_value' => $contact_tracker_profile['position'] ? $contact_tracker_profile['position'] : 'bottom',
    );
    
    // Put submit button at the end.
    $form['submit']['#weight'] = 101;
    
    // Add custom submit function.
    $form['#submit'][] = 'contact_tracker_contact_admin_edit_submit';
  }
  
  // Alter form contact-mail-page.
  if ('contact_mail_page' === $form_id) {
    
    // Prepend custom submit function. Make sure that our function runs before default one.
    array_unshift($form['#submit'], 'contact_tracker_contact_mail_page_submit');
  }
}

/**
 * Custom submit function of form "contact-admin-edit". 
 */
function contact_tracker_contact_admin_edit_submit($form, &$form_state) {
  $contact_tracker_options = array_filter(array_values($form_state['values']['contact_tracker']['options']));
  $contact_tracker_position = $form_state['values']['contact_tracker']['position'];
  
  // Set contact tracker profile.
  contact_tracker_set_profile(
    $form_state['values']['cid'],
    array('options' => $contact_tracker_options, 'position' => $contact_tracker_position)
  );
}

function contact_tracker_contact_mail_page_submit($form, &$form_state) {
  $contact_tracker_profile = contact_tracker_get_profile($form_state['values']['cid']);
  
  // Append tracker information one by one.
  $info = array();
  foreach ($contact_tracker_profile['options'] AS $module) {
    $function = $module . '_contact_tracker_message';
    if (function_exists($function) && $message = call_user_func($function)) {
      $info[] = $message;
    }
  }
  
  $separator_above = "\n\n\n" . variable_get('ct_separator_above', t(CT_SEPARATOR_ABOVE)) . "\n\n";
  $separator_below = "\n\n" . variable_get('ct_separator_below', t(CT_SEPARATOR_BELOW)) . "\n\n\n";
  
  if ($contact_tracker_profile['position'] == 'top') {
    $form_state['values']['message'] = $separator_above . implode("\n\n", $info) . $separator_below . $form_state['values']['message'];
  }
  else {
    $form_state['values']['message'] .= $separator_above . implode("\n\n", $info) . $separator_below;
  }
}

/**
 * API for getting contact tracker profile(s).
 * 
 * @param $cid
 *   Contact form catetory id. Pass 0 to get all profiles.
 */
function contact_tracker_get_profile($cid = 0) {
  return contact_tracker_set_profile($cid);
}

/**
 * API for setting contact tracker profile.
 * 
 * @param $cid
 *   contact form catetory id
 *   
 * @param $profile
 *   profile to be set
 */
function contact_tracker_set_profile($cid = 0, $profile = NULL) {
  static $contact_tracker_profiles = NULL;
  
  if (NULL === $contact_tracker_profiles || NUll !== $profile) {
    $contact_tracker_profiles = variable_get('contact_tracker_profiles', array());
    
    // Set profile if $profile is given.
    if (is_numeric($cid) && NULL !== $profile) {
      $contact_tracker_profiles[$cid] = $profile;
      variable_set('contact_tracker_profiles', $contact_tracker_profiles);
    }
  }
  
  return 0 === $cid ? $contact_tracker_profiles : $contact_tracker_profiles[$cid];
}


